xttitle (1.2.0-2) unstable; urgency=medium

  * debian/watch: fix url to https://github.com/mojotx/xttitle/tags.

 -- Paulo Roberto Alves de Oliveira (aka kretcheu) <kretcheu@gmail.com>  Sat, 22 Oct 2022 23:56:00 -0300

xttitle (1.2.0-1) unstable; urgency=medium

  * New upstream release.
  * d/watch: Update to use normal mode.
  * d/patches/01-fix-makefile.patch: Refresh, mostly merged upstream.

 -- Paulo Roberto Alves de Oliveira (aka kretcheu) <kretcheu@gmail.com>  Mon, 28 Mar 2022 20:23:00 -0300

xttitle (1.1~git20181024.b52256-3) unstable; urgency=medium

  * d/rules: Remove -pie and -fPIE from compiler option.
    Disables checking for PIE which is automatically applied by Debian's GCC
    and no longer requires a compiler command line argument.

 -- Paulo Roberto Alves de Oliveira (aka kretcheu) <kretcheu@gmail.com>  Wed, 09 Feb 2022 10:15:00 -0300

xttitle (1.1~git20181024.b52256-2) unstable; urgency=medium

  * d/watch: Update to use git mode.

 -- Paulo Roberto Alves de Oliveira (aka kretcheu) <kretcheu@gmail.com>  Wed, 26 Jan 2022 12:31:00 -0300

xttitle (1.1~git20181024.b52256-1) unstable; urgency=medium

  * New upstream release.
  * d/control:
    - Bump Standards-Version to 4.6.0.1.
    - Include Rules-Requires-Root field.
  * - Include VCS-* fields.
  * d/copyright:
    - Update years and use https instead http.
    - Upstream change license to GPL-3+.
  * d/docs: rename README file by upstream.
  * d/install: to include binary file.
  * d/manpages: To include upstream manpage and remove maintainer manpage.
  * d/p/01-fix-makefile.patch:
    - Fix Makefile to solve hardening problems (Closes: #939676).
  * d/rules: Include flags to solve hardening problems.
  * d/u/metadata: Include upstream metadata.
  * d/watch: Update to use upstream github repository.

 -- Paulo Roberto Alves de Oliveira (aka kretcheu) <kretcheu@gmail.com>  Tue, 11 Jan 2022 11:14:00 -0300

xttitle (1.0-7) unstable; urgency=medium

  * New maintainer. (Closes: #792561)
  * Migrations:
      - debian/copyright to 1.0 format.
      - DebSrc to 3.0 format.
      - DebHelper level to 9.
  * debian/control:
      - Bumped Standards-Version to 3.9.6.
      - Add Homepage and include ${misc:Depends} in Depends.
  * debian/rules:
      - accommodate to new standards.
  * debian/watch
      - Upstream don't provide one, so use fake watch until there.
  * Corrected name of manpage.

 -- Paulo Roberto Alves de Oliveira (aka kretcheu) <kretcheu@gmail.com>  Mon, 20 Jul 2015 12:31:00 -0300

xttitle (1.0-6) unstable; urgency=medium

  * Orphaning package.
  * Changed Maintainer address to QA.

 -- Alberto Gonzalez Iniesta <agi@inittab.org>  Wed, 15 Jul 2015 10:58:23 +0000

xttitle (1.0-5) unstable; urgency=low

  * Changed maintainer email address.
  * Bumped Standards-Version to 3.6.1.0, no change.
  * Moved binary and man page from /usr/X11R6/ to /usr/ as per
    policy 11.8.7.

 -- Alberto Gonzalez Iniesta <agi@inittab.org>  Thu, 24 Mar 2005 20:17:48 +0100

xttitle (1.0-4) unstable; urgency=low

  * Made x-terminal-emulator a Suggests: instead of a Recommends:
    (Closes: #191515)

 -- Alberto Gonzalez Iniesta <agi@agi.as>  Sat,  7 Jun 2003 18:30:33 +0200

xttitle (1.0-3) unstable; urgency=low

  * New maintainer. (Closes: #190813)
  * Bumped Standards-Version to 3.5.9, no change.
  * Included .bashrc example from Chris Lawrence (Closes: #118064)

 -- Alberto Gonzalez Iniesta <agi@agi.as>  Wed,  7 May 2003 21:12:12 +0200

xttitle (1.0-2) unstable; urgency=low

  * Debian QA Upload: package orphaned.
  * Changed Maintainer to Debian QA.
  * Removed emacs voodoo from debian/changelog.
  * Acknowledge NMU closed bugs:
    - Fixed building with sudo. Closes: #119095
  * Upstream source URL from debian/copyright no
    longer works.  Didn't find a replacement.
  * Get rid of copyright-lists-upstream-authors-with-dh_make-boilerplate
    by changing 'Upstream Author(s)' to 'Upstream Author'
    in debian/copyright.  There is only one Upstream Author.

 -- Peter Palfrader <weasel@debian.org>  Sat, 26 Apr 2003 07:23:39 +0200

xttitle (1.0-1.1) unstable; urgency=low

  * NMU
  * Fixed building with sudo. Closes: #119095

 -- Scott M. Dier <sdier@debian.org>  Sun,  3 Feb 2002 15:00:39 -0600

xttitle (1.0-1) unstable; urgency=low

  * Initial Debian release.

 -- Dafydd Harries <daf@parnassus.ath.cx>  Tue,  7 Aug 2001 15:09:37 +0100
